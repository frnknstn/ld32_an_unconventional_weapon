#!/usr/bin/python
# coding=utf-8

"""Ludum Dare Base Code

"""

from __future__ import division
from __future__ import print_function

import os
import sys
import random
import time
import itertools
from math import sqrt, pow

import pygame

from defines import *
from level import Level
import planet_mob
from viewport import Viewport
import sprite_types
import player_mob
import resource
import interface

class FramerateCounter(object):
    buffer_size = 60
    
    def __init__(self):
        self.rates = [0 for i in range(self.buffer_size)]
        self.average = 0.0
        self.last_time = time.clock()
        self.index = 0
        self.zero_interval_frames = 0
        self.font = pygame_default_font(16)

    def frame(self):
        """signal the next frame to the FPS counter, and return the current average framerate"""
        if sys.platform.startswith('linux'):
            this_time = time.time()
        else:
            this_time = time.clock()
        this_interval = (this_time - self.last_time)

        if this_interval == 0:
            # Too short a time between frames, or insufficient timer resolution
            # Don't log this frame
            self.zero_interval_frames += 1
            return self.average

        this_rate = (1 + self.zero_interval_frames) / (this_time - self.last_time)
        self.zero_interval_frames = 0

        # circular buffer for frame rates
        self.index += 1
        if self.index >= self.buffer_size:
            self.index = 0
        self.rates[self.index] = this_rate

        self.last_time = this_time
        self.average = sum(self.rates) / self.buffer_size

        return self.average

    def draw(self, surf):
        surf.blit(self.font.render("FPS: %0.3f" % self.average, True, (0xFF, 0xFF, 0xFF), (0,0,0)), (0, 0))


class Game(object):
    def __init__(self):
        self.set_state("config")
        self.player_score = 0
        self.oblivion_score = 0

        self.level = 1

        self.running = True

    def main(self):
        global frame_count

        debug("Game.main() running")
        pygame.mixer.music.load("music/music.ogg")
        pygame.mixer.music.play(-1)


        # allow the user to select doublesize mode
        if self.state == "config":
            global DOUBLE_SIZE
            DOUBLE_SIZE = False
            screen = pygame.display.set_mode(SCREEN_SIZE)
            self.config_screen(screen)
            self.set_state("pre-game")

        # set up double-size, if needed
        if not DOUBLE_SIZE:
            screen = pygame.display.set_mode(SCREEN_SIZE)
        else:
            self.real_screen = pygame.display.set_mode(
                (SCREEN_SIZE[0] * 2, SCREEN_SIZE[1] * 2))
            screen = pygame.Surface(SCREEN_SIZE)
        self.screen = screen

        self.clock = pygame.time.Clock()

        sprite_types.load_sprite_files()

        self.running = True
        self.set_state("intro")

        while self.running:
            if self.state == "intro":
                self.intro_screen(screen)
                self.flash_screen(screen, "main")
            elif self.state == "main":
                debug("level %d" % self.level)
                if self.level == 1:
                    self.starting_planets = 400
                else:
                    self.starting_planets = int(self.player_score / PLANET_COST)
                self.starting_stars = int(self.starting_planets / 50)
                self.main_game(screen)
            elif self.state == "post-level":
                # post-game interlude
                self.flash_screen(screen, "scores")
            elif self.state == "scores":
                self.score_screen(screen)
                self.flash_screen(screen, "main")
                self.level += 1

        debug("Leaving main loop")
        screen.fill((0, 0, 0))

        self.flip()

    def main_game(self, screen):
        """Run the main game"""
        level = Level("map/map.tmx")
        tiles = level.images

        viewports = []
        players = []
        suns = []
        planets = []
        sprites = []        # all sprites and things, including the players

        pygame.INFO_SPRITE_COUNT = 0

        player_score = 0.0
        oblivion_score = 0.0
        score_font = pygame_default_font(36)
        level_age = 0

        # prepare the stuff
        player = player_mob.PlayerMob((200, 200), "right", sprites=sprites)
        suns.append(player)
        players.append(player)
        sprites.append(player)

        # populate the level
        def make_random_planet():
            pos = None
            planet = None
            while pos is None or planet.rect.collidelist(planets) != -1:
                pos = (random.randrange(level.size[0]), random.randrange(level.size[1]))
                planet = planet_mob.PlanetMob(pos)
            planets.append(planet)
            sprites.append(planet)

        def make_random_sun():
            pos = None
            sun = None
            while pos is None or sun.rect.collidelist(planets) != -1:
                pos = (random.randrange(level.size[0]), random.randrange(level.size[1]))
                sun = sprite_types.SunMob(pos)
            suns.append(sun)
            sprites.append(sun)

        for i in range(self.starting_planets):
            make_random_planet()

        for i in range(self.starting_stars):
            make_random_sun()

        # prepare the GUI
        viewport = Viewport(level, (0, 0), (0, 0), (SCREEN_SIZE[0], SCREEN_SIZE[1]), player)
        viewport.draw(screen, sprites)
        move = (0, 0)

        viewports.append(viewport)

        age_interface = AgeInterface(screen, pygame.Rect(SCREEN_SIZE[0]-128, SCREEN_SIZE[1]-64, 112, 48))

        self.flip()

        frame_count = 0
        fps = FramerateCounter()
        info_font = pygame_default_font(16)

        # Main Loop
        self.set_state("main")

        while self.running:
            # parse events and input
            self.parse_events(handler=player.handle_input)

            # respawn if dead
            if player.dead:
                player.move_to(200, 200)
                player.spawn()
                sprites.append(player)

            player.parse_input()

            # Update all the sprites
            for sprite in sprites:
                sprite.update_animation()

            # Remove dead sprites
            old_sprites_count = len(sprites)
            sprites = [ x for x in sprites if not x.dead ]
            if len(sprites) != old_sprites_count:
                debug("Removed %d dead sprites" % (old_sprites_count - len(sprites)))

            player.herd_children(sprites)
            for planet in planets:
                planet.herd_children(sprites)

            self.move_sprites(sprites, level)

            # heat up the sun
            player.temperature += 1

            # work out what roughly what planet the mouse is over
            if DEBUG:
                planet_info = ""
                selected_planet = None
                this_port = viewports[0]
                mouse_pos = pygame.mouse.get_pos()
                if DOUBLE_SIZE:
                    mouse_pos = (mouse_pos[0] / 2, mouse_pos[1] / 2)

                cursor_x = this_port.rect.left + mouse_pos[0]
                cursor_y = this_port.rect.top + mouse_pos[1]
                cursor_pos = (cursor_x, cursor_y)

                if len(planets) == 1:
                    selected_planet = planet
                else:
                    for planet in planets:
                        if planet.rect.collidepoint(cursor_pos):
                            selected_planet = planet
                            break


            # heat loss calculation
            # q = σ * T^4 * A         (1)
            #
            # where
            #
            # q = heat transfer per unit time (W)
            # σ = 5.6703 10-8 (W/m2K4) - The Stefan-Boltzmann Constant
            # T = absolute temperature Kelvin (K)
            # A = area of the emitting body (m2)
            #
            # heat transfer: inverse square:
            # 1 / (distance^2)

            # heat the planets
            for sun in suns:
                for planet in planets:
                    distance = sqrt(pow(sun.pos[0] - planet.pos[0], 2) + pow(sun.pos[1] - planet.pos[1], 2))
                    planet.frame_heating += 1 / pow(distance, 2) * sun.temperature

            for planet in planets:
                # cool the planets
                planet.frame_cooling = SBC * (pow(planet.temperature, 4) - AMBIENT_TEMPERATURE_4) * (1/1800)

                if DEBUG and planet == selected_planet:
                    planet_info = "distance: %.03f pop: %d (%+.3f) planet: %d C (%.01f) heat: %.05f cool: %.05f" % (
                         distance, int(planet.population), planet.frame_population_change,
                         int(planet.temperature - 273.15), planet.temperature,
                         planet.frame_heating, planet.frame_cooling
                         )
                # apply the heat
                planet.temperature = min(max(0, planet.temperature + planet.frame_heating - planet.frame_cooling), 3000)
                planet.frame_heating = 0

                # update the population
                planet.update_population()

                # update scores
                if planet.frame_population_change < 0:
                    if planet.overheat:
                        player_score += -planet.frame_population_change
                    else:
                        oblivion_score += -planet.frame_population_change


            # Update all the viewports
            for viewport in viewports:
                # move viewport if needed
                viewport.move_view_by(*move)

                viewport.update()
                viewport.draw(screen, sprites, level.triggers)

            # draw score
            score_text = "Souls collected: %d" % int(player_score)
            score_surface = score_font.render(score_text, True, (255,0,0))
            screen.blit(score_surface, (0, SCREEN_SIZE[1] - 64))

            score_text = "Oblivion: %d" % int(oblivion_score)
            score_surface = score_font.render(score_text, True, (0,0,255))
            screen.blit(score_surface, (0, SCREEN_SIZE[1] - 32))

            level_age += 1
            progess = level_age / LEVEL_DURATION
            age_interface.age = progess
            if level_age > LEVEL_DURATION:
                self.set_state("post-level")
                self.player_score = player_score
                self.oblivion_score = oblivion_score

            # interfaces
            for item in interface.interface_list:
                item.draw()

            frame_count += 1
            fps.frame()

            if DEBUG:
                # draw debug info
                fps.draw(screen)
                info = "%dx%d window, %d viewports, %d sprites (%d drawn), %d frames" % (
                    SCREEN_SIZE[0], SCREEN_SIZE[1], len(viewports), len(sprites), pygame.INFO_SPRITE_COUNT,
                    frame_count
                    )
                screen.blit(info_font.render(info, True, (255,255,255), (0,0,0)), (0, 12))
                pygame.INFO_SPRITE_COUNT = 0

                screen.blit(info_font.render(planet_info, True, (255,255,255), (0,0,0)), (0, 24))

            self.flip()

            if self.state != "main":
                # leave the main loop
                break

        # exit main game loop func


    def flip(self, surface=None):
        """Move the screen surface to the actual display"""
        if surface is None:
            surface = self.screen

        if DOUBLE_SIZE:
            pygame.transform.scale(surface, (SCREEN_SIZE[0] * 2, SCREEN_SIZE[1] * 2), self.real_screen)

        pygame.display.flip()

        if CAP_FPS is not None:
            self.clock.tick(CAP_FPS)
        else:
            # be a good multitasking program buddy
            time.sleep(0.0005)

    def move_sprites(self, sprites, level):

        for sprite in sprites:
            if not isinstance(sprite, sprite_types.Mob):
                continue

            # apply physics

            # apply physics - air resistance
            dr = sprite.dr
            if dr != 0 and sprite.mu_coefficient != 0:
                # faster objects have more turbulence
                speed_scale = (dr / RESISTANCE_SCALE)

                dr -= AIR_RESISTANCE * sprite.mu_coefficient * speed_scale

                sprite.set_move_vector(sprite.theta, dr)

            sprite.move(level)

    def set_state(self, state):
        assert(state in GAME_STATES)
        debug("Game state changed to '%s'" % state)
        self.state = state

    def parse_events(self, handler=None):
        """Parse the PyGame event queue, handling global keys, or passing them off to a handler otherwise"""

        def dummy_handler(event):
            """dummy event handler"""
            return False

        if handler is None:
            handler = dummy_handler

        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key in (pygame.K_ESCAPE, pygame.K_q):
                    self.running = False
                elif not handler(event):
                    debug(event)

            elif event.type == pygame.KEYUP:
                handler(event)

            elif event.type == pygame.QUIT:
                self.running = False


    def score_screen(self, screen):
        """show the end-of-game scores"""

        # handler to allow player to exit the screen
        def handler(event):
            if event.type == pygame.KEYDOWN:
                self.skip_ahead = True
        self.skip_ahead = False

        # determine the winner
        if self.player_score > self.oblivion_score:
            max_score = self.player_score
            winner_text = "Sun God!!!?"
        elif self.player_score == self.oblivion_score:
            max_score = self.player_score
            winner_text = "Tie!!!?"
        else:
            max_score = self.oblivion_score
            winner_text = "Oblivion. Oblivion always wins."

        if max_score == 0:
            # prevent division by zero later
            max_score = 1


        if self.player_score >= PLANET_COST:
            event_handler = handler
            epilogue_text_1 = "But at least you can take these souls"
            epilogue_text_2 = "and form a new, smaller universe."
            footer_text = "(press any key to try the next level, or Q to quit)"
        else:
            # game over :(
            event_handler = None
            epilogue_text_1 = "You no longer have enough souls to form a"
            epilogue_text_2 = "single planet. The universe is at an end."
            footer_text = "(press Q or Esc to quit)"


        # draw the scores
        screen.fill((0,0,0))
        header = pygame_default_font(36).render("Final scores:", True, (255,255,255))
        oblivion_header = pygame.font.Font("fonts/Inconsolata-Regular.ttf", 32).render("Oblivion: %d" % int(self.oblivion_score), True, (0,0,255))
        player_header = pygame.font.Font("fonts/Inconsolata-Regular.ttf", 32).render(  "Sun God:  %d" % int(self.player_score), True, (255,0,0))
        winner_header = pygame_default_font(36).render("Winner: " + winner_text, True, (255,255,0))
        epilogue_header_1 = pygame_default_font(36).render(epilogue_text_1, True, (255,255,0), (0,0,0))
        epilogue_header_2 = pygame_default_font(36).render(epilogue_text_2, True, (255,255,0), (0,0,0))

        footer = pygame_default_font(22).render(footer_text, True, (185,185,185), (0,0,0))

        screen.blit(header, (64, 64))
        screen.blit(oblivion_header, (64, 128))
        screen.blit(player_header, (64, 192))
        screen.blit(winner_header, (88, 272))

        # center the footer
        footer_x = 0.5 * (SCREEN_SIZE[0] - footer.get_width())
        screen.blit(footer, (footer_x, SCREEN_SIZE[1] - 24))


        part_width = 3
        bar_width = int((SCREEN_SIZE[0] - 64 - 64 )/ part_width)
        bar_count = 0
        oblivion_bar = pygame.Surface((part_width, 32))
        oblivion_bar.fill((0,0,255))
        oblivion_bar_parts = int(bar_width * self.oblivion_score / max_score)
        player_bar = pygame.Surface((part_width, 32))
        player_bar.fill((255,0,0))
        player_bar_parts = int(bar_width * self.player_score / max_score)

        self.flip()

        epilogue_alpha = 0
        epilogue_mask_1 = epilogue_header_1.copy()
        epilogue_mask_2 = epilogue_header_2.copy()
        epilogue_mask_1.fill((0,0,0))
        epilogue_mask_2.fill((0,0,0))

        while self.running and not self.skip_ahead:
            self.parse_events(event_handler)

            epilogue_alpha += 1
            epilogue_header_1.set_alpha(epilogue_alpha)
            epilogue_header_2.set_alpha(epilogue_alpha)
            screen.blit(epilogue_mask_1, (88, 304))
            screen.blit(epilogue_header_1, (88, 304))
            screen.blit(epilogue_mask_2, (88, 336))
            screen.blit(epilogue_header_2, (88, 336))

            if bar_count <= bar_width:
                if bar_count <= oblivion_bar_parts:
                    screen.blit(oblivion_bar, (64 + (bar_count * part_width), 160))
                if bar_count <= player_bar_parts:
                    screen.blit(player_bar, (64 + (bar_count * part_width), 224))
                bar_count += 1

                self.flip()


    def config_screen(self, screen):
        """show the story"""
        # handler to allow player to exit the screen
        def handler(event):
            if event.type == pygame.KEYDOWN:
                global DOUBLE_SIZE
                if event.key == pygame.K_1:
                    DOUBLE_SIZE = False
                    self.skip_ahead = True
                elif event.key == pygame.K_2:
                    DOUBLE_SIZE = True
                    self.skip_ahead = True
        self.skip_ahead = False

        # draw the story
        screen.fill((0,0,0))
        header = pygame_default_font(52).render("Configuration", True, (255,0,0), (0,0,0))
        screen.blit(header, (64, 64))

        story = """
Press 2 for double-size window mode (~1080p)
Press 1 for normal size
"""

        interface.draw_text(story, screen, pygame.Rect(64, 128, SCREEN_SIZE[0] - 64*2, SCREEN_SIZE[1]), size=14)

        footer_text = "(press 1 or 2, or Q to quit)"
        footer = pygame_default_font(22).render(footer_text, True, (185,185,185), (0,0,0))

        # center the footer
        footer_x = 0.5 * (SCREEN_SIZE[0] - footer.get_width())
        screen.blit(footer, (footer_x, SCREEN_SIZE[1] - 24))

        # can't use the standard stuff this warly
        pygame.display.flip()

        while self.running and not self.skip_ahead:
            self.parse_events(handler)
            time.sleep(0.01)


    def intro_screen(self, screen):
        """show the story"""

        # handler to allow player to exit the screen
        def handler(event):
            if event.type == pygame.KEYDOWN:
                self.skip_ahead = True
        self.skip_ahead = False

        # draw the story
        screen.fill((0,0,0))
        header = pygame_default_font(52).render("Weapon Against Oblivion", True, (255,0,0), (0,0,0))

        story = \
"The people of the universe cried out in anguish. Their universe was dying, the cold god Oblivion had taken hold. As " \
"the last suns were dying out, and all heat was slowly being devoured, the people prayed for a WEAPON to fight back. " \
"They prayed to the one god they needed most, The eldest god, who they had almost forgotten.\n\n" \
"When the sun rose the next day, it was with purpose. The sun god, incarnated is his most pure form, came to beat " \
"back Oblivion.\n\n" \
"INSTRUCTIONS:\n\n" \
"" \
"Harvest the souls of the people throughout the universe.\n" \
"You earn the souls of people who die in fire; Oblivion takes those who die of cold.\n" \
"The population will grow while the temperature is in a healthy range.\n" \
"Your sun is dying, as it ages it gets hotter.\n" \
"Keys: \n" \
"      Up Arrow / Down Arrow: speed up, slow down\n" \
"      Left Arrow / Right Arrow: steer\n"

        interface.draw_text(story, screen, pygame.Rect(64, 128, SCREEN_SIZE[0] - 64*2, SCREEN_SIZE[1]), size=14)

        footer_text = "(press any key to skip, or Q to quit)"
        footer = pygame_default_font(22).render(footer_text, True, (185,185,185), (0,0,0))

        # center the footer
        footer_x = 0.5 * (SCREEN_SIZE[0] - footer.get_width())
        screen.blit(footer, (footer_x, SCREEN_SIZE[1] - 24))

        self.flip()

        header_alpha = 0
        header_mask = header.copy()
        header_mask.fill((0,0,0))

        while self.running and not self.skip_ahead:
            self.parse_events(handler)

            header_alpha += 1
            header.set_alpha(header_alpha)
            screen.blit(header_mask, (64, 64))
            screen.blit(header, (64, 64))

            if header_alpha <= 255:
                self.flip()


    def flash_screen(self, screen, next_state):
        """Draw a whole-screen flash transition"""
        flash_surface = pygame.Surface(SCREEN_SIZE)
        flash_surface.fill((255,255,255))

        back_buffer = pygame.Surface(SCREEN_SIZE)
        back_buffer.blit(screen, (0,0))

        frames = 0
        flash_duration = 40

        while self.running:
            self.parse_events()

            screen.blit(back_buffer, (0,0))

            flash_surface.set_alpha(int(frames / flash_duration * 255))
            screen.blit(flash_surface, (0,0))

            self.flip()

            frames += 1
            if frames > flash_duration:
                break

        self.set_state(next_state)


class AgeInterface(interface.InterfaceItem):
    def __init__(self, surf, rect):
        interface.InterfaceItem.__init__(self, surf, rect)

        self.font = pygame_default_font(18)
        self.title = self.font.render("Sun Age:", True, (255,255,255))

        self.dead_age_box = pygame.Surface((16, 8))
        self.dead_age_box.fill((255,0,0))

        self.age = 0.0

        self.age_box = pygame.Surface((16, 8))
        self.age_box.fill((255,255,0))

    def draw(self):
        age_color = max(0, 255 - int(self.age * 255))
        self.age_box.fill((255, age_color, 0))

        origin = self.rect.topleft
        self.surf.blit(self.title, origin)
        self.surf.blit(self.dead_age_box, (origin[0], origin[1] + 16))
        self.surf.blit(self.age_box, (origin[0] + 16, origin[1] + 16))
        self.surf.blit(self.dead_age_box, (origin[0] + 32, origin[1] + 16))


def main():
    try:
        pygame.init()

        resource.load_resources()

        game = Game()
        game.main()

    finally:
        debug("Stopping pygame...")
        pygame.quit()

    debug("Exiting.")


if __name__ == "__main__":
    main()
