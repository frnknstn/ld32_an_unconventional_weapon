## Weapon Against Oblivion #

The people of the universe cried out in anguish. Their universe was dying, the cold god Oblivion had taken hold. As the 
last suns were dying out, and all heat was slowly being devoured, the people prayed for a WEAPON to fight back. They 
prayed to the one god they needed most, The eldest god, who they had almost forgotten.

When the sun rose the next day, it was with purpose. The sun god, incarnated is his most pure form, came to beat back 
Oblivion. 

## How to run from source #

 1. Install Python 2.7 from here:
    https://www.python.org/downloads/release/python-279/
 2. Install PyGame 1.9.2. If you are on Windows, you can find a recent-ish build here:
    https://bitbucket.org/pygame/pygame/downloads/pygame-1.9.2a0.win32-py2.7.msi
 3. Double-click 'game.py'

Also feel free to check out the git repo on Bitbucket:
https://bitbucket.org/frnknstn/ld32_an_unconventional_weapon
