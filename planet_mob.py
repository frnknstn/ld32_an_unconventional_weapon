#!/usr/bin/python
from __future__ import division
from __future__ import print_function

import random
from math import e, pow, pi

import pygame

from defines import *

import sprite_types

class PlanetMob(sprite_types.Mob):
    def __init__(self, pos, direction="none"):
        sprite_types.Mob.__init__(self, "planet", pos, direction)

        self.fire_frames = self.get_frames("fire")
        self.ice_frames = self.get_frames("ice")

        self.child_sprites = []

        self.base_image = None
        self.base_fire = None
        self.last_update_temperature = 0

        self.food = 0
        self.population = random.randrange(0, 25000)
        self.research = 0

        self.temperature = 372
        self.frame_heating = 0
        self.frame_cooling = 0
        self.sterile = False

        self.frame_population_change = 0
        self.overheat = True    # true if deaths are due to high temp, false if due to low temp

        # surface to keep track of this planet's population
        self.population_counter = None
        self.font = pygame_default_font(14)

        self.start_anim(self.anim_idle())

    def update_population(self):
        """Update the population of the planet"""
        if self.sterile:
            self.frame_population_change = 0
            return
        elif self.temperature >= STERILE_TEMPERATURE:
            self.overheat = True
            self.frame_population_change = -self.population
            self.population = 0
            self.sterilise()
        elif self.population == 0:
            return
        else:
            # normal temperature calculations
            t = self.temperature
            p = self.population

            if t >= 263 and t < 373:
                # growth range: 263K - 373K
                temperature_multiplier = 1
                pop_change = 10
            elif t > 373:
                # too hot
                temperature_multiplier = min((t - 373) / 100 + 0.5, 15)
                pop_change = -10
                self.overheat = True
            else:
                # too cold
                temperature_multiplier = min((263 - t) / 100 + 0.1, 1)
                pop_change = -10
                self.overheat = False


            sigma_p = p * 8 / 5000
            pop_multiplier = 1 / (1 + pow(e, -(sigma_p - 5)))
            pop_change = pop_change * pop_multiplier * temperature_multiplier

            self.population += pop_change
            self.frame_population_change = pop_change

        if self.population < 1:
            self.population = 0

    def get_population_counter(self):
        """return a surface with the population number drawn on in"""
        population = self.population
        if population >= 1000000:
            text = "%.1fM" % (population / 1000000)
        elif population >= 1000:
            text = "%.1fK" % (population / 1000)
        else:
            text = "%d" % int(population)

        if self.frame_population_change >= 0:
            color = (0,255,0)
        elif self.overheat:
            color = (255,0,0)
        else:
            color = (0,0,255)

        return self.font.render(text, False, color)

    def sterilise(self):
        """Too much heat has rendered this a dead world, do the VFX"""
        self.sterile = True
        self.start_anim(self.anim_sterile())
        for i in range(4):
            self.child_sprites.append(SmokeMob(self.pos))


    def heat_mask(self):
        """Apply the fire / ice effect to our image"""
        self.image = self.base_image.copy()
        if self.temperature > 303.15:
            self.fire_image.set_alpha(min(255, (self.temperature - 303.15) / 200 * 255))
            self.image.blit(self.fire_image, (0,0))
        elif self.temperature < 290.15:
            self.ice_image.set_alpha(min(255, (290.15 - self.temperature) / 100 * 255))
            self.image.blit(self.ice_image, (0,0))

        self.last_update_temperature = self.temperature

    def herd_children(self, sprites):
        """Move our child sprites into the main sprite list"""
        sprites.extend(self.child_sprites)
        self.child_sprites = []

    def anim_idle(self):
        """Animation coroutine for standing animation"""
        frames = self.get_frames("stand")
        fire_frame_count = len(self.fire_frames)
        ice_frame_count = len(self.ice_frames)

        # stagger the sprite update intervals
        anim_stagger = random.randrange(60)

        while True:
            for frame in frames:
                self.base_image = frame
                self.base_fire = self.fire_frames[0]
                self.base_ice = self.ice_frames[0]

                self.fire_image = self.base_fire.copy()
                self.ice_image = self.base_ice.copy()

                for delay in range(60):
                    if anim_stagger > 0:
                        anim_stagger -= 1
                        continue

                    if abs(self.last_update_temperature - self.temperature) > 1.3:
                        self.heat_mask()
                        yield True
                    else:
                        yield False

    def anim_sterile(self):
        """Animation coroutine for dead planets"""
        frames = self.fire_frames

        while True:
            for frame in frames:
                self.image = frame
                yield True

                for delay in range(59):
                    yield False


class SmokeMob(sprite_types.Mob):
    """Puff of smoke"""
    def __init__(self, pos, direction="none", angle=None):

        self.alpha = 190

        direction = random.choice(("left", "right"))
        sprite_types.Mob.__init__(self, "smoke", pos, direction)

        if angle is None:
            angle = random.random() * pi * 2

        self.set_move_vector(angle, 0.3)
        self.start_anim(self.anim_walk())

    def anim_walk(self):
        """Animation coroutine for smoke clouds"""
        frames = self.get_frames("walk")

        while True:
            for frame in frames:
                self.image = frame.copy()

                for delay in range(100):
                    self.image.set_alpha(self.alpha)
                    self.alpha -= 1
                    yield True


            self.dead = True
