#!/usr/bin/python
from __future__ import division
from __future__ import print_function

import pygame.font as pygame_font_module      # avoid polluting other module's namespaces
from pygame.rect import Rect

DEBUG = False
#DEBUG = True

BENCHMARK = False

RESOURCE_CATEGORIES = ("tiles", "interface", "icons", "music", "sound")
SPRITE_DIR = "sprites"
CAP_FPS = None
#CAP_FPS = 20
CAP_FPS = 60
#CAP_FPS = 90
#CAP_FPS = 250

DOUBLE_SIZE = False
DOUBLE_SIZE = True

GAME_STATES = {"config", "pre-game", "intro", "main", "post-level", "scores"}

DRAW_MAP = True
#DRAW_MAP = False

# physics
AIR_RESISTANCE = 0.033
RESISTANCE_SCALE = 2
# interface layout

SCREEN_SIZE = (896, 504)
#SCREEN_SIZE = (2896, 1004)

TILE_SIZE = 32

DIRECTIONS = ("none", "up", "right", "down", "left")

DEFAULT_TEXTBLOCK_FONT = "fonts/DejaVuSans.ttf"

# game constants
STERILE_TEMPERATURE = 573

SBC = 5.6703e-8             # The Stefan-Boltzmann Constant
AMBIENT_TEMPERATURE = 130
AMBIENT_TEMPERATURE_4 = AMBIENT_TEMPERATURE*AMBIENT_TEMPERATURE*AMBIENT_TEMPERATURE*AMBIENT_TEMPERATURE

LEVEL_DURATION = 7200   # frames

PLANET_COST = 10000

# functions
def debug(*args):
    if DEBUG:
        print(*args)

def pygame_default_font(size):
    """Emulate the output of pygame.font.SysFont("", size), but in a way that doesn't crash if run through cx_Freeze"""
    
    # reduce the size to match what pygame's font.c:font_init() does
    # reportedly, this is to keep the modern pygame default font roughly the same size as that of older pygame versions.
    size = int(size * 0.6875)
    if size < 1:
        size = 1
    
    return pygame_font_module.Font("fonts/freesansbold.ttf", size)


if __name__ == "__main__":
    import game
    game.main()
