##Ideas#

racing game
mega racing game, across planets

Nuclear / chem / bio warfare

Falling buildings

Breed creatures to 
 * commit crimes
 * conquer the world
 * tamagochi?



YOU ARE THE POWER SOURCE

You are the sun, the God to the people of the solar system, given sentience by the prayers of your people. Now you must protect them from intergalactic invaders and destroy their enemies, while keeping their planets alive.


##Art requirements#

 * Sun
  * Angry sun?
 * Planets
  * Class M
  * Ice effect
  * desert effect
  * Rock?
  * Gas giant?
 * Background Stars and nebula
 * Enemies
  * space ships
   * Friendly
   * Enemy
  * Aliens?

Black of space: 0xff070918

##Layout#

896x504 means doubled 1792x1008

16:9

Sun 48x48

##Gameplay#

###Things?#

* sun flies around
 * cool further away from sun, warm if too close
 * healthy planets make stuff
 * change heat?
* gravity? Grab planets?
* civilisation?

##Wolfram formula #

graph Piecewise[{ {y = -(290.15 - x) / 100 * 255, x < 290.15}, {(x - 303.15) / 200 * 255, x>303.15} }]



## Day 2 #

Todo:

 * stop overlapping planets
 * blow up planets
 * population
 * soul / score count
 * sun leaves the map?
 * level migration
 * frozen planets
 * intro?
 * music
 * sound? In space??
 * menu?
 * cool vfx?
 * level layouts?
 * options?
 * more art?
 
Final Todo:
 * intro
 * double size?
 * music
 * Commit
 * Readme
 * Upload
  